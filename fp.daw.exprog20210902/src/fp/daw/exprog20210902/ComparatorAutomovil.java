package fp.daw.exprog20210902;

import java.util.Comparator;

	public class ComparatorAutomovil implements Comparator<Automovil> {
		@Override
		public int compare(Automovil arg0, Automovil arg1) {
			if (arg0.getMotor().toString().compareTo(arg1.getMotor().toString())==0) {
				return (arg0.getPotencia()-arg1.getPotencia());
			} else {
				return arg0.getMotor().toString().compareTo(arg1.getMotor().toString());
			}
		}

}
