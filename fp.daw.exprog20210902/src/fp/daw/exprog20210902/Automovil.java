package fp.daw.exprog20210902;

public class Automovil implements Comparable<Automovil>{
	private String marca;
	private String modelo;
	private String matricula;
	private Integer aniofab;
	public enum Motor {GASOLINA, DIESEL, HIBRIDO, ELECTRICO;

	String compareTo(String name) {
		// TODO Auto-generated method stub
		return null;
	}};
	
	private Motor motor;
	private Integer potencia;
	
	public Automovil(String marca, String modelo, String matricula, Integer aniofab, Motor motor, Integer potencia) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.matricula = matricula;
		this.aniofab = aniofab;
		this.motor = motor;
		this.potencia = potencia;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public Integer getAniofab() {
		return aniofab;
	}

	public void setAniofab(Integer aniofab) {
		this.aniofab = aniofab;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Motor getMotor() {
		return motor;
	}

	public void setMotor(Motor motor) {
		this.motor = motor;
	}
	
	public Integer getPotencia() {
		return potencia;
	}

	public void setPotencia(Integer potencia) {
		this.potencia = potencia;
	}

	@Override
	public int compareTo(Automovil arg0) {

		if (marca.compareTo(arg0.getMarca())==0) {
			if (modelo.compareTo(arg0.getModelo())==0) {
				return motor.toString().compareTo(arg0.getMotor().toString());
			} else {
				return modelo.compareTo(arg0.getModelo());
			}
		} else {
			return marca.compareTo(arg0.getMarca());
		}
	}

	@Override
	public String toString() {
		return "\nAutomovil [ " + marca + " " + modelo + " matricula=" + matricula + ", ao " + aniofab
				+ " motor=" + motor + " potencia=" + potencia + "]" ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result + ((motor == null) ? 0 : motor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Automovil other = (Automovil) obj;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (motor != other.motor)
			return false;
		return true;
	}

}

