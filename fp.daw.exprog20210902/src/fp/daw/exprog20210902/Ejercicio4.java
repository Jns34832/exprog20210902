package fp.daw.exprog20210902;

import java.util.*;

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Queue<Integer> cola=new LinkedList<>();
		cola.offer(1);
		cola.offer(8);
		cola.offer(7);
		cola.offer(2);
		cola.offer(9);
		cola.offer(18);
		cola.offer(12);
		cola.offer(0);
		
		System.out.println(cola);
		separarcola(cola);
	}

	private static void separarcola(Queue<Integer> cola) {
		Queue<Integer> colapar=new LinkedList<>();
		Deque<Integer> colaimpar1=new LinkedList<>();
		colapar.add(cola.poll());
		
		int i=1;
		while (!cola.isEmpty()) {
			Integer elemento=cola.poll();
			if (i%2==0) {
				colapar.offer(elemento);
			} else {
				colaimpar1.push(elemento);
			}
			i++;
		}

		while (!colapar.isEmpty() && !colaimpar1.isEmpty()) {
			cola.offer(colapar.poll());
			cola.offer(colaimpar1.poll());
		}
		System.out.println(cola);
		
	}
}
