package fp.daw.exprog20210902;

import java.util.*;
import java.util.Map.Entry;

public class Ejercicio2 {

	public static void main(String[] args) {
	//	{Ana=22, Luis=25, Pablo=25, Elena=20, Alicia=20, C�sar=20, Lara=25, Tom�s=25, V�ctor=22}
		Map<String, Integer> alumnos=new TreeMap<>();
		alumnos.put("Ana", 22);
		alumnos.put("Luis", 25);
		alumnos.put("PAblo", 25);
		alumnos.put("Elena", 20);
		alumnos.put("Alicia", 20);
		alumnos.put("Cesar", 20);
		alumnos.put("Lara", 25);
		alumnos.put("Tomas", 25);
		alumnos.put("Victor", 22);
		
		Integer edadmenosfrecuente=buscaedad(alumnos);
		System.out.println(edadmenosfrecuente);


	}

	private static Integer buscaedad(Map<String, Integer> alumnos) {
		Map<Integer,Integer> cuenta=new TreeMap<Integer,Integer>();
		Integer conta=0;
		for(Entry<String, Integer> mp: alumnos.entrySet()) {
		//	String clave=mp.getKey(); 
			Integer valor=mp.getValue();
		
			if (cuenta.containsKey(valor)) {
				conta=cuenta.get(valor)+1;
			}
			else {
				conta=1;
			}
			cuenta.put(valor, conta);
		}
		
		Integer menor=Integer.MAX_VALUE;
		Integer edadmenor=0;
		
		for(Entry<Integer, Integer> mp: cuenta.entrySet()) {
			if (mp.getValue()<menor) {
				menor=mp.getValue();
				edadmenor=mp.getKey();
			}
		}
		
		return edadmenor;
	}
	

}
