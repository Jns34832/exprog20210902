package fp.daw.exprog20210902;

import java.io.*;
import java.util.*;

public class ParqueMovil implements Serializable {
	
private Map<String, TreeMap<String, Integer> > general=new TreeMap<String, TreeMap<String, Integer>>();
	
	public ParqueMovil(String fichero) {
	
	try {
		FileInputStream file=new FileInputStream(fichero);
		InputStreamReader obj=new InputStreamReader(file);
		BufferedReader reg= new BufferedReader(obj);
		
		String encabezado=reg.readLine(); //guardamos la primera linea, que son los encabezados, en un string
		String[] primera=encabezado.split(";");
				
		String l;
		while ((l=reg.readLine())!=null) {
			String[] linea=l.split(";");
			TreeMap<String,Integer> provincias=new TreeMap<String, Integer>();
			for (int i = 1; i < linea.length; i++) {
				provincias.put(primera[i] ,Integer.parseInt(linea[i]));
			}
			System.out.print(linea[0]+"\t\t");
			System.out.println(provincias);
			 general.put(linea[0], provincias);
		}
		reg.close();
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
}
	

}
