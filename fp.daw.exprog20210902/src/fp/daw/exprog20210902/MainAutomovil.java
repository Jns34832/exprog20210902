package fp.daw.exprog20210902;

import java.util.*;

import fp.daw.exprog20210902.Automovil.Motor;

public class MainAutomovil {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Set <Automovil> automovil=new TreeSet<>();
		
		
		Set<Automovil> automovil=new HashSet <Automovil>();
		automovil.add(new Automovil("seat","panda","0001AAA", 2000, Motor.GASOLINA, 125));
		automovil.add(new Automovil("seat","panda","0002AAA", 1970, Motor.ELECTRICO, 75));
		automovil.add(new Automovil("volkswagen","golf","0004MLD", 1999, Motor.DIESEL, 150));
		automovil.add(new Automovil("renault","megane","0003BBB", 2010, Motor.HIBRIDO, 100));
		automovil.add(new Automovil("jeep","wrangler","0005CCC", 2020, Motor.ELECTRICO, 200));
		

		System.out.println(automovil.toString());
		System.out.println("ARRAY 1");
		
		Automovil[] autoarray=new Automovil[automovil.size()];
		int i=0;
		for (Automovil array : automovil) {
			autoarray[i]=array;
			i++;
		}
		System.out.println(Arrays.toString(autoarray));
		
		System.out.println("ARRAY 2");
		Arrays.sort(autoarray, new ComparatorAutomovil());
		System.out.println(Arrays.toString(autoarray));
		

	}

}
